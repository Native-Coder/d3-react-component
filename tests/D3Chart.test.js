/* eslint-disable testing-library/no-node-access */
/* eslint-disable testing-library/no-container */
import React  from "react";

// import revenratorRuntime from "regenerator-runtime/runtime";
import { D3Chart} from "../src/D3Chart.js";
import { render /*fireEvent, cleanup*/ } from "@testing-library/react";
import { /*NoCreate, NoUpdate,*/ TestChart } from "./TestingCharts.js";

//global.d3    = jest.fn();
const testId = "test_id";
console.warn = jest.fn();

test( "data defaults to an empty array", () => {
	global.d3 = jest.fn();
	let reference = React.createRef();
	render(
		<TestChart
			id={testId}
			ref={reference}
		/>
	);
	expect( Array.isArray( reference.current.data ) ).toBe( true );
	expect( reference.current.data.length ).toBe( 0 );
});

test( "passes canvasNode, data, and props to charts create method", () => {

	let reference = React.createRef();
	let data = { foo: "bar" };
	render(
		<TestChart
			id={testId}
			data={data}
			ref={reference}
			topMargin={30}
			bottomMargin={30}
			leftMargin={30}
			rightMargin={30}
			myCustomProp="foo"
		/>
	);
	let chart = reference.current;
	expect( chart.data.foo ).toBe( "bar" );
	expect( chart.canvas instanceof HTMLElement ).toBe( true );
	expect( chart.props.topMargin ).toBe( 30 );
	expect( chart.props.bottomMargin ).toBe( 30 );
	expect( chart.props.leftMargin ).toBe( 30 );
	expect( chart.props.rightMargin ).toBe( 30 );
	expect( chart.props.myCustomProp ).toBe( "foo" );
});

test( "passes new data to charts update method instead of re-rendering the component", () => {

	let reference = React.createRef();
	const { /*container,*/ rerender } = render(
		<TestChart
			id={testId}
			data={{ foo: "bar" }}
			ref={reference}
		/>
	);
	expect( reference.current.data.foo ).toBe( "bar" );

	rerender(
		<TestChart
			id={testId}
			data={{ bar: "baz" }}
			ref={reference}
		/>
	);
	expect( reference.current.data.bar ).toBe( "baz" );

});
//
// test( "destroy method is called before unmount", () => {
// 	const { unmount } = render(
// 		<TestChart
// 			id={testId}
// 		/>
// 	);
// 	unmount();
// 	expect( console.warn.mock.calls ).toBe( "destroy called" );
// });

test ( "generates a default id and uses it for the rendered div", () => {

	let reference = React.createRef();
	const { container } = render(
		<TestChart
			ref={reference}
		/>
	);

	expect(
		container.querySelector( `#${reference.current.id}` )
			instanceof HTMLDivElement
	).toBe( true );

});

test( "constructor throws error if D3 is not loaded", () => {
	delete window.d3;
	expect( () => {
		new D3Chart();
	}).toThrow( "D3 is not loaded" );
});


test( "default margins are all 0", () => {
	global.d3 = jest.fn();
	const instance = new TestChart({ id: "testId" });
	expect( instance.margin.top    ).toBe( 0 );
	expect( instance.margin.left   ).toBe( 0 );
	expect( instance.margin.right  ).toBe( 0 );
	expect( instance.margin.bottom ).toBe( 0 );
});

test( "default size is 500px X 500px", () => {
	global.d3 = jest.fn();
	const instance = new TestChart({ id: "testId" });
	expect( instance.totalWidth ).toBe( 500 );
	expect( instance.totalHeight ).toBe( 500 );
});

// test( "default destroy method removes d3 root svg element from dom", () => {
// 	global.d3 = jest.fn();
// 	const instance = new TestChart({ id: "testId" });
// 	expect( instance.destroy() ).toBe( null );
// });

test( "can override top margin", () => {
	global.d3 = jest.fn();
	const instance = new TestChart({
		id: "testId",
		topMargin: 50
	});
	expect( instance.margin.top ).toBe( 50 );
});

test( "can override left margin", () => {
	global.d3 = jest.fn();
	const instance = new TestChart({
		id: "testId",
		leftMargin: 50
	});
	expect( instance.margin.left ).toBe( 50 );
});

test( "can override bottom margin", () => {
	global.d3 = jest.fn();
	const instance = new TestChart({
		id: "testId",
		bottomMargin: 50
	});
	expect( instance.margin.bottom ).toBe( 50 );
});

test( "can override right margin", () => {
	global.d3 = jest.fn();
	const instance = new TestChart({
		id: "testId",
		rightMargin: 50
	});
	expect( instance.margin.right ).toBe( 50 );
});

test( "can override width", () => {
	global.d3 = jest.fn();
	const instance = new TestChart({
		id: "testId",
		width: 1000
	});
	expect( instance.totalWidth ).toBe( 1000 );
});

test( "can override height", () => {
	global.d3 = jest.fn();
	const instance = new TestChart({
		id: "testId",
		height: 1000
	});
	expect( instance.totalHeight ).toBe( 1000 );
});

test( "overriding height or width does not change the other", () => {
	global.d3 = jest.fn();
	let instance = new TestChart({
		id: "testId",
		height: 1000
	});

	expect( instance.totalWidth  ).toBe( 500 );
	expect( instance.totalHeight ).toBe( 1000 );

	instance = new TestChart({
		id: "testId",
		width: 1000
	});

	expect( instance.totalHeight ).toBe( 500 );
	expect( instance.totalWidth  ).toBe( 1000 );
});

test( "correctly computes height/width with respect to margins", () => {
	global.d3 = jest.fn();
	let instance = new TestChart({
		id: "testId",
		height: 1000,
		topMargin: 100,
		bottomMargin: 100
	});

	expect( instance.height ).toBe( 800 );

	instance = new TestChart({
		id: "testId",
		width: 1000,
		leftMargin: 100,
		rightMargin: 100
	});

	expect( instance.width ).toBe( 800 );
});

test( "totalWidth and totalHeight are unaffected by margins", () => {
	global.d3 = jest.fn();
	let instance = new TestChart({
		id: "testId",
		height: 1000,
		topMargin: 100,
		bottomMargin: 100
	});

	expect( instance.totalHeight ).toBe( 1000 );

	instance = new TestChart({
		id: "testId",
		width: 1000,
		leftMargin: 100,
		rightMargin: 100
	});

	expect( instance.totalWidth ).toBe( 1000 );
});
