
import D3Chart from "../src/D3Chart.js";

/**
 * This class is a test class used by D3Canvas.test.js
 * It is used to verify that D3Canvas is calling the
 * proper functions at the prope times
 */
export class TestChart extends D3Chart {
	constructor( props ){
		super( props );
		this.props = props;
		this.createCalled  = false;
		this.updateCalled  = false;
		this.create  =  this.create.bind( this );
		this.destroy = this.destroy.bind( this );
	}

	create( canvas, data, props ){
		this.canvas = canvas;
		this.data = data;
		this.props = props;
		this.createCalled = true;
	}

	update( data ){
		this.data = data;
		this.updateCalled = true;
	}

	destroy(){
		console.warn( "destroy called" );
	}
}


export class NoCreate {
	constructor( props ){}

	update( data ){}

	destroy(){}
}

export class NoUpdate {
	constructor( props ){}

	create( canvas, data, props ){}

	destroy(){}
}

export default {
	NoCreate, NoUpdate, TestChart
};
