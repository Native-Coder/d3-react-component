import React from "react";
/**
 * A parent class for any D3 charts that will be using the D3Canvas Component
 */
export default class D3Chart extends React.Component {
	constructor( props = {}){
		super( props );

		if( !window.d3 )
			throw new Error( "D3 is not loaded" );

		this.d3 = window.d3;

		/**
		 * Default to no margin; Highly recommended to just override in child
		 * constructor
		 */
		this.margin  = {
			top    : 0,
			right  : 0,
			bottom : 0,
			left   : 0
		};

		/**
		 * override the defaults:
		 * NOTE: allows end user to use either margin<side> OR <side>Margin to
		 * specify its value
		 */
		if( this.props.marginTop || this.props.topMargin )
			this.margin.top = ( this.props.marginTop || this.props.topMargin );
		if( this.props.marginRight || this.props.rightMargin )
			this.margin.right  = ( this.props.marginRight || this.props.rightMargin );
		if( this.props.marginBottom || this.props.bottomMargin )
			this.margin.bottom = ( this.props.marginBottom || this.props.bottomMargin );
		if( this.props.marginLeft || this.props.leftMargin )
			this.margin.left = ( this.props.marginLeft || this.props.leftMargin );

		this.totalWidth  = ( this.props.width || 500 );
		this.totalHeight = ( this.props.height || 500 );
		this.width = this.totalWidth - this.margin.left - this.margin.right;
		this.height = this.totalHeight - this.margin.top  - this.margin.bottom;

		this.id   = ( props.id    ||    this._id() );
		this.data = ( props.data ? props.data : [] );
		// Pass chart-specific properties on to the chart via the charts required
		// create method
		this._props = { ...props };
		delete this._props.data;  // Not chart-specific
		delete this._props.chart; // Not chart-specific

		// Bind the proper context
		this.create  =  this.create.bind( this );
		this.destroy = this.destroy.bind( this );
		this._id     =     this._id.bind( this );

		// Because we are using the passed in ID for the div wrapper,
		// we need to generate a unique id for the d3 class to consume
		this._props.id = this._id();
	}

	/**
	 * Override this method to Initialize the chart
	 */
	create( canvasNode, data = [], otherProps = {}){
		throw new TypeError( "Children of D3Chart must have a create method" );
	}

	/**
	 * Override this method to render the chart based on the provided data.
	 */
	update( data = [] ){
		throw new TypeError( "Children of D3Chart must have an update method" );
	}


	/**
	 * Destroys the chart
	 */
	destroy(){
		if( this.svg )
			this.svg.remove();
	}

	/**
	 * Generate the id for this object
	 *	@returns - A unique-enough string
	 */
	_id(){
		return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace( /[xy]/g, function( c ) {
			var r = Math.random()*16|0, v = c == "x" ? r : ( r&0x3|0x8 );
			/**
			 * Ensure that the first character in the generated id is non-numeric. DOM ID 
			 * selectors cannot start with a numeric character
			 */
			return "id_" + v.toString( 16 ); 
		});
	}

	// Create the D3 chart
	componentDidMount(){
		this.create(
			this.canvas,
			this.data,
			this.props // In case there are any chart-specific props
		);
	}

	// Update the D3 chart
	shouldComponentUpdate( newProps, newState ){
		// Update the data if we received new props
		if( this.data !== newProps.data ){
			this.data = newProps.data;
			this.update( this.data );
		}

		return false; // Prevents React from re-rendering the canvas
	}

	// Let D3 clean up before unmounting the DOM
	componentWillUnmount(){
		this.destroy();
	}

	_setRef( componentNode ){
		this.canvas = componentNode;
	}

	render(){
		return (
			<div
				id={this.id}
				ref={this._setRef.bind( this )}
				style={{width: this.width, height: this.height}}
				className="d3-canvas"
				role="application"
			/>
		);
	}
}

export { D3Chart };
