import D3Chart from "d3-react-component";

/* eslint-disable no-mixed-spaces-and-tabs */
export default class LineChart extends D3Chart {
	/**
	 * Create and store the necessary functions for your vis
	 * @param canvasNode - The DOM node that will contain your vis.
	 * @param data - The data passed to the vis when it was created
	 * @param props - all properties that you passed to your vis
	 */
	async create( canvasNode, data = [], props = {}){
		this.data = this.d3.csvParse( data );
		console.log( this.data );

		// Append the SVG element
		this.svg = this.d3.select( canvasNode )
		  .append( "svg" )
			.attr( "width", this.totalWidth )
			.attr( "height", this.totalHeight )
		  .append( "g" )
			.attr( "transform", "translate(" + this.margin.left + "," + this.margin.top + ")" );

		// Create scales
		this.x = this.d3.scaleLinear().range( [ 0, this.width ] );
		this.y = this.d3.scaleLinear().range( [ this.height, 0 ] );

		// Create path generator
		this.pathGenerator = this.d3.line()
			.x( d => this.x( d.year ) )
			.y( d => this.y( d.megawatts ) );

		this.tooltip = this.d3.select( canvasNode )
		  .append( "div" )
			.attr( "class", "tooltip" )
			// NOTE: This should really be in a seperate CSS file.
			// It's done here for brevity and clarity
			// Best practice is to Only use D3 to set styles that are derived
			// from the data source, and use CSS for static and initial styles
			.style( "opacity", 0 )
			.style( "position", "absolute" )
			.style( "background", "lightgreen" )
			.style( "text-align", "center" )
			.style( "border-radius", "8px" )
			.style( "padding", "2px" )
			.style( "pointer-events", "none" );

		this.formatAxis = this.d3.format( ".0f" );

		this.xAxis = this.svg.append( "g" )
			.attr( "transform", `translate( 0 , ${this.height} )` );
		this.yAxis = this.svg.append( "g" )
			.attr( "trasnform", `translate( ${this.width}, 0 )` );

		this.line = this.svg.append( "path" ).attr( "id", `${this.id}_line` );

		// Force initial render
		this.update( this.data );
	}

	/**
	 * Anytime the D3Canvas component is updated with new data, this
	 * function is called. use it to update/transition your vis
	 * @param data - The new data
	 */
	update( data = [] ){
		// format data
		data.forEach( d => {
			d.year      = +d.year;
			d.megawatts = +d.megawatts;
			d.turbines  = +d.turbines;
			d.per_bine  = +d.per_bine;
		});
		this.data = data;

		this.x.domain( this.d3.extent( data, d => d.year ) );
		this.y.domain( [ 0, this.d3.max( data, d => d.megawatts ) ] );

		// Add the path.
		this.line.data( [ this.data ] )
			.attr( "d", this.pathGenerator )
			.style( "fill", "none" )
			.style( "stroke", "black" );

		//Insert Circles
		this.svg.selectAll( ".circle" )
			.data( this.data )
		  .enter().append( "circle" )
			.attr( "r", 6 )
			.attr( "cx", d => this.x( d.year ) )
			.attr( "cy", d => this.y( d.megawatts ) )
			.style( "fill", d => {
				if( d.per_bine > 1.5 ) {
					return "green";
				} else if( d.per_bine > 1 ) {
					return "blue";
				} else if( d.per_bine > .5 ) {
					return "orange";
				}
				return "red";
			})
			.on( "mouseover", ( e, d ) => {
				this.tooltip.style( "opacity", .9 );
				this.tooltip.html(
					`MegaWatts: ${d.megawatts}</br>` +
					`Turbines: ${d.turbines}</br>` +
					`MW/T: ${d.per_bine}`
				)
					.style( "left", `${e.pageX}px` )
					.style( "top", `${e.pageY}px` );
			})
			.on( "mouseout", d => {
				this.tooltip.style( "opacity", 0 );
			});

		// Update the axis
		this.xAxis.call( this.d3.axisBottom( this.x ).tickFormat( this.formatxAxis ) );
		this.yAxis.call( this.d3.axisLeft( this.y ) );
	}
}
